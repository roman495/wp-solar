<?php

get_header();

get_template_part( 'template-parts/section', 'about' );

get_template_part( 'template-parts/section', 'services' );

get_footer();