<?php
$query = new WP_Query([
    'post_type'      => 'services',
    'orderBy'        => 'menu_order',
    'order'          => 'ASC',
    'posts_per_page' => 6,
]);
if( $query->have_posts() ): ?>
<div class="icons wrap">
    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
        <div class="icons__element">
            <div class="icons__img">
                <?php the_post_thumbnail( 'full' ) ?>
            </div>
            <?php
                the_title( '<p class="icons__title">', '</p>' );
                the_content();
            ?>
        </div>
    <?php endwhile ?>
    <?php wp_reset_postdata() ?>
</div>
<?php endif ?>