<?php
/**
 * solar functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package solar
 */

require get_template_directory() . '/SolarHeaderMenu.php';

function dd($data)
{
    echo '<pre style="text-align: left; font-size: 1em; background-color: #263238; color: #4caf50">' . print_r($data, 1) . '</pre>';
}

add_action('admin_menu', function() {
    add_menu_page(
        __( 'Sections', 'solar' ),
        __( 'Sections', 'solar' ),
        'manage_options',
        'sections',
        'add_my_setting',
        'dashicons-admin-page',
        6
    );
});

function law_slides_posttype()
{
    register_post_type( 'services', [
            'labels'        => [
                'name'          => __( 'Services', 'solar' ),
                'singular_name' => __( 'Services', 'solar' ),
            ],
            'public'        => true,
            'has_archive'   => true,
            'rewrite'       => true,
            'show_in_menu'  => 'sections',
            'supports'      => [
                'title',
                'editor',
                'thumbnail',
				'page-attributes'
            ],
        ]
    );
}
add_action( 'init', 'law_slides_posttype', 0 );

add_filter( 'manage_services_posts_columns', function ( $columns ) {
	$my_columns = [
		'order'	=> __( 'Order', 'solar' ),
		'icon' 	=> __( 'Icon', 'solar' ),
	];
	return array_slice( $columns, 0, 1 ) 
			+ $my_columns 
			+ array_slice( $columns, 1, 1 )
			+ [ 'text' => __( 'Text', 'solar' ) ] 
			+ $columns;
} );

add_filter( 'manage_services_posts_custom_column', function ( $column_name, $post_ID ) {
	if ( $column_name === 'order' ) {
		echo $post_ID;
	} elseif ( $column_name === 'text' ) {
		echo the_content();
	} elseif ( $column_name === 'icon' && has_post_thumbnail() ) {
		echo '<a href="' . get_edit_post_link() . '">';
		the_post_thumbnail( 'icon' );
		echo '</a>';
	}
	return $column_name;
}, 10, 2 );

add_action( 'admin_print_footer_scripts-edit.php', function () {
	echo <<<EOT
	<style>
		.column-order { width: 50px; }

		.column-title { width: 300px }

		.column-icon { width: 100px; }

		.icon.column-icon a { display: inline-block; }
	</style>
EOT;
} );


if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'solar_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function solar_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on solar, use a find and replace
		 * to change 'solar' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'solar', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_image_size( 'icon', 64, 64 );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'solar' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'solar_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'solar_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function solar_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'solar_content_width', 640 );
}
add_action( 'after_setup_theme', 'solar_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function solar_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'solar' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'solar' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'solar_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function solar_scripts() {
	wp_enqueue_style( 'solar-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'solar-style', 'rtl', 'replace' );

	wp_enqueue_style( 'solar-googlefont', 'https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap' );
	wp_enqueue_style( 'solar-core', get_template_directory_uri() . '/assets/css/core.css', array(), _S_VERSION );
	wp_enqueue_style( 'solar-header', get_template_directory_uri() . '/assets/css/blocks/header.css', array(), _S_VERSION );
	wp_enqueue_style( 'solar-button', get_template_directory_uri() . '/assets/css/blocks/button.css', array(), _S_VERSION );
	wp_enqueue_style( 'solar-about-us', get_template_directory_uri() . '/assets/css/blocks/about-us.css', array(), _S_VERSION );
	wp_enqueue_style( 'solar-icons', get_template_directory_uri() . '/assets/css/blocks/icons.css', array(), _S_VERSION );

	wp_enqueue_script( 'jquery' );

	wp_enqueue_script( 'solar-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'solar_scripts' );

function shortcode_span( $atts, $content )
{
	return "<span>$content</span>";
}
add_shortcode( 'span', 'shortcode_span' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

