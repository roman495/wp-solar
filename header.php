<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package solar
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div class="container">
	<div class="header"
		<?php 
			$bg_img = get_theme_mod( 'solar_background' );
			if ( !empty( $bg_img ) ) {
				echo ' style="background: url(' . $bg_img . ') center no-repeat; background-size: cover;"';
			}
		?>
	>
		<header class="wrap header__wrap">
			<div class="header__top">
				<div class="header__logo">
					<p>
						<a href="<?php bloginfo( 'url' ) ?>"><?php bloginfo() ?></a>
					</p>
				</div>
				<?php wp_nav_menu([
					'theme_location' => 'menu-1',
					'container'		 => false,
					'items_wrap'	 => '%3$s',
					'walker'		 => new SolarHeaderMenu
				]) ?>
			</div>
			<h1><?php echo do_shortcode( get_bloginfo( 'description' ) ) ?></h1>
		</header>
	</div>
