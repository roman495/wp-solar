<?php
$query = new WP_Query([
    'pagename'       => 'about-us',
    'posts_per_page' => 1,
]);
if( $query->have_posts() ) : $query->the_post(); ?>
<div class="about-us wrap">
    <div class="about-us__img">
        <?php the_post_thumbnail( 'full' ) ?>
    </div>
    <div class="about-us__text">
        <p class="about-us__title"><?php the_title() ?></p>
        <?php the_content() ?>
        <div class="button about-us__button">
            <a href="<?php the_permalink() ?>"><?php _e( 'Download Broshure', 'solar' ) ?></a>
        </div>
    </div>
</div>
<?php wp_reset_postdata() ?>
<?php endif ?>
