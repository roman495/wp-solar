<?php
/**
 * solar Theme Customizer
 *
 * @package solar
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function solar_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'solar_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'solar_customize_partial_blogdescription',
			)
		);
	}
	
	$wp_customize->add_section(
		'solar_settings',
		[
			'title'			=> __( 'Solar settings', 'solar' ),
			'description'	=> __( 'Settings for Solar theme', 'solar' ),
			'priority'		=> 1,
		]
	);
	
	$setting = 'solar_background';
	
	$wp_customize->add_setting(
		$setting,
		[
			'default'	=> '',
			'transport' => 'postMessage'
			// 'transport' => 'refresh'
		]
	);
	
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			$setting,
			[
				'section'  => 'solar_settings',
				'label'	   => __( 'Background image', 'solar' ),
				'settings' => $setting
			]
		)
	);

	if ( isset( $wp_customize->selective_refresh ) ){

		$wp_customize->selective_refresh->add_partial( $setting, [
			'selector'            => '.header',
			'container_inclusive' => false,
			'render_callback'     => 'footer_inner_dh5theme',
			'fallback_refresh'    => false, // Prevents refresh loop when document does not contain .cta-wrap selector. This should be fixed in WP 4.7.
		] );

		// поправим стиль, чтобы кнопку было видно
		add_action( 'wp_head', function(){
			echo '<style>.header .customize-partial-edit-shortcut{ margin: 10px 0 0 38px; }</style>';
		} );

	}

}
add_action( 'customize_register', 'solar_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function solar_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function solar_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function solar_customize_preview_js() {
	wp_enqueue_script( 'solar-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), _S_VERSION, true );
}
add_action( 'customize_preview_init', 'solar_customize_preview_js' );
